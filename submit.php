<!DOCTYPE HTML>  
<html>
<head>
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$name1Err = $healthid1Err = $name2Err = $healthid2Err = $name3Err = $healthid3Err = "";
$name1 = $healthid1 = $name2 = $healthid2 = $name3 = $healthid3 = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name1"])) {
    $name1Err = "Name1 is required";
  } else {
    $name1 = test_input($_POST["name1"]);
  }
  
  if (empty($_POST["healthid1"])) {
    $healthid1Err = "Healthid1 is required";
  } else {
    $healthid1 = test_input($_POST["healthid1"]);
  }
    
  if (empty($_POST["name2"])) {
    $name2Err = "Name2 is required";
  } else {
    $name2 = test_input($_POST["name2"]);
  }
   if (empty($_POST["healthid2"])) {
    $healthid2Err = "Healthid2 is required";
  } else {
    $healthid2 = test_input($_POST["healthid2"]);
  }
  if (empty($_POST["name3"])) {
    $name3Err = "Name3 is required";
  } else {
    $name2 = test_input($_POST["name3"]);
  }
   if (empty($_POST["healthid2"])) {
    $healthid3Err = "Healthid3 is required";
  } else {
    $healthid3 = test_input($_POST["healthid3"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<div class="form_block">
<h2>PHP Form Validation Example</h2>
<p><span class="error">* Required field</span></p>
<form method="post" id="datafrm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Name1: <input type="text" name="name1" id="name1">
  <span class="error">* <?php echo $name1Err;?></span>
  <br><br>
  Healthid1: <input type="text" name="healthid1" id="healthid1" class="allownumericwithoutdecimal">
  <span class="error">* <?php echo $healthid1Err;?></span>
  <br><br>
   Name2: <input type="text" name="name2" id="name2">
  <span class="error">* <?php echo $name2Err;?></span>
   <br><br>
   Healthid2: <input type="text" name="healthid2" id="healthid2" class="allownumericwithoutdecimal">
  <span class="error">* <?php echo $healthid2Err;?></span>
  <br><br>
   Name3: <input type="text" name="name3" id="name3">
  <span class="error">* <?php echo $name3Err;?></span>
   <br><br>
   Healthid3: <input type="text" name="healthid3" id="healthid3" class="allownumericwithoutdecimal">
  <span class="error">* <?php echo $healthid3Err;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>
</div>
<div id="error"></div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$( "#datafrm" ).submit(function( event ) {
 if($.isNumeric( $('#healthid1').val()) && $.isNumeric( $('#healthid2').val()) && $.isNumeric( $('#healthid3').val())
	 && ($('#healthid1').val().length >= 5) && ($('#healthid2').val().length >= 5) && ($('#healthid3').val().length >= 5) 
    && ($('#name1').val().length >3) && ($('#name2').val().length >3) && ($('#name3').val().length >3) )
 {
	alert($('#name1').val().length);
	alert($('#healthid1').val().length);
	$(".form_block").html("SUCCESS");
 }
 else{
	 $(".form_block").css("display","none");
	 $("#error").html("ERROR");
 }
  event.preventDefault();
});

$(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
</script>


</body>
</html>