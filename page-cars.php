<?php
/**
 * Exit if accessed directly.
 *
 * @package Responsive
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Full Content Template
 *
Template Name:  Automobile Template
 *
 * @file           page-cars.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>
<div id="carspage_block">
	<div class="automobile_list">
		<?php 
							global $post;
							
								$postslist = get_posts(array(
									'posts_per_page' => -1,
									'post_type' =>'automobiles',
								));	
							
							if ($postslist) {
									foreach ( $postslist as $postdata ) :
										setup_postdata( $postdata );
																									
										?>		
										<div class="automobile_overallblock">
											<div class="automobile_contentblock">
												<div class="automobile_contentblock_padding">
												<a href="<?php echo get_permalink($postdata->ID);?>" title="<?php echo get_permalink($postdata->ID);?>"><h3><?php echo get_the_title($postdata->ID);?></h3></a>
													
												</div>	
																								
											</div>
										</div>
							<?php 
								endforeach; 
								wp_reset_postdata();
							}
							else{
								echo "<li class='norelatedposts'>There are currently no Related Posts available.</li>";
							}
						?>
		
	</div>
</div>
<?php get_footer(); ?>

